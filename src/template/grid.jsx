import React, { Component } from 'react';

export default class Grid extends Component {

    //converter no padrao de 12 col do bootstrap col-...
    toCssClasses(numbers) {
        const cols = numbers ? numbers.split(' ') : []
        let classes = ''

        if(cols[0]) classes += `col-xs-${cols[0]}`
        if(cols[1]) classes += ` col-sm-${cols[1]}`
        if(cols[2]) classes += ` col-md-${cols[2]}`
        if(cols[3]) classes += ` col-lg-${cols[3]}`
        
        return classes
    }

    render() {
        //se o col não for setado entao é 12
        const gridClasses = this.toCssClasses(this.props.cols || 12)

        return (
            <div className={gridClasses}>
                { this.props.children }
            </div>
        )
    }
}