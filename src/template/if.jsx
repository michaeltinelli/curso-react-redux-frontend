import React from 'react';

export default props => {
    
    if (props.test) {
        //retorne o obj que esta dentro da tag <If />
        return props.children;
    } else {
        return false;
    }
}