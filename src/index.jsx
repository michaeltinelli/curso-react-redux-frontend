import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware,createStore } from 'redux';
import { Provider } from 'react-redux';

import promise from 'redux-promise';
//permite que uma action executa outra
import multi from 'redux-multi';
import thunk from 'redux-thunk';

import App from './main/app';
import reducers from './main/reducers';

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

//vai criar o objeto state
const store = applyMiddleware(thunk,multi,promise)(createStore)(reducers, devTools)

ReactDOM.render(
   <Provider store={store}>
     <App />
   </Provider> 
    , document.getElementById('app'));