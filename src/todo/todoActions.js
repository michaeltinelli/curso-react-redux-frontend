import axios from 'axios'

const URL = 'http://localhost:3003/api/todos'

//retorna o objeto la no reducer
export const changeDescription = event => ({
    type: 'DESCRIPTION_CHANGED',
    payload: event.target.value
})

export const search = () => {
    //uma promise
    const req = axios.get(`${URL}?sort=-createdAt`)
    return {
        type: 'TODO_SEARCHED',
        payload: req
    }
}

/*
export const add = (description) => {
    const request = axios.post(URL, { description })
    return [
        {
        type: 'TODO_ADDED',
        payload: request
    }, search() ] // esses [] são do middleware multi para poder executar uma action dentro de uma
}
*/

export const add = (description) => {
    return dispatch => {
        axios.post(URL, { description })
            //.then(resp => dispatch({ type: 'TODO_ADDED', payload: resp.data })) //1° vai la no reducer e apagar o description
            .then(resp => dispatch(clear()))
            .then(resp => dispatch(search())) // (then comentado acima) 2° depois chamar o metodo search
    }
}

export const markAsDone = todo => {
    return dispatch => {
        axios.put(`${URL}/${todo._id}`, { ...todo, done: !todo.done })
            //.then(resp => dispatch({ type: 'TODO_MARKED_AS_DONE', payload: resp.data }))
            .then(resp => dispatch(search()))
    }
}

/*
export const markAsPending = todo => {
    return dispatch => {
        axios.put(`${URL}/${todo._id}`, { ...todo, done: false })
            //.then(resp => dispatch({ type: 'TODO_MARKED_AS_PENDING', payload: resp.data }))
            .then(resp => dispatch(search()))
    }
}
*/

export const remove = todo => {
    //retorno uma function que tem como parametro um dispatch // idem aos demais metodos
    return dispatch => {
        axios.delete(`${URL}/${todo._id}`)
            .then(resp => dispatch(search()))
    }
}

export const clear = () => {
    return { type: 'TODO_CLEAR' }
}