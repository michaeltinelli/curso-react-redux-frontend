
const INITIAL_STATE = {
    description: '',
        list: []
}

//recebe o state atual e uma action. sempre q a action for selecionada os reducers
// da app são chamados e vai querer mudar o estado dentre de um reducer

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'DESCRIPTION_CHANGED':
            return { ...state, description: action.payload }
        case 'TODO_SEARCHED':
            //o payload.data ainda não esta pronto. vai ter o middleware
            return { ...state, list: action.payload.data }
        //case 'TODO_ADDED':
        case 'TODO_CLEAR':
            return { ...state, description: '' }
        default:
            return state
    }
}